const { setHeadlessWhen } = require('@codeceptjs/configure')

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS)

exports.config = {
	tests: './*_test.js',
	output: './output',
	helpers: {
		Puppeteer: {
			url: 'http://localhost',
			show: true,
			windowSize: '375x812',
			isMobile: true,
			userAgent:
				'Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1',
			waitForNavigation: 'networkidle0',
			waitForAction: 500,
			waitForTimeout: 2000
		}
	},
	include: {
		I: './steps_file.js'
	},
	bootstrap: null,
	mocha: {},
	name: 'avito-test',
	plugins: {
		pauseOnFail: {},
		retryFailedStep: {
			enabled: true
		},
		tryTo: {
			enabled: true
		},
		screenshotOnFail: {
			enabled: true
		}
	},
	timeout: 30000
}
