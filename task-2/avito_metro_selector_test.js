/// <reference path="./steps.d.ts" />

Feature('Фильтр выбора метро')

Before(({ I }) => {
	I.amOnPage('https://m.avito.ru/moskva/kommercheskaya_nedvizhimost')
	I.wait(2)
	I.waitForElement('[data-marker="search-bar/filter"]', 10)
	I.click('[data-marker="search-bar/filter"]')
	I.wait(1)
})

Scenario('Проверка наличия фильтра', ({ I }) => {
	I.say('Проверяю наличия фильтра выбора метро в Москве')
	I.seeElement('[data-marker="metro-select/withoutValue"]')

	I.say('Проверяю отсутствие фильтра метро в Чите')
	I.click('[data-marker="location-chooser"]')
	I.fillField('[data-marker="region-search-bar/search"]', 'Чита')
	I.click('Чита', '[data-marker="region(661950)/names"]')
	I.wait(2)
	I.dontSeeElement('[data-marker="metro-select/withoutValue"]')
})

Scenario('Проверка переключения между Алфавит/Линия', ({ I }) => {
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Выбираем станцию метро "Академическая"')
	I.click('Академическая')

	I.say('Переключаюсь с Алфавита на Линию')
	I.click('[data-marker="metro-select-dialog/tabs/button(lines)"]')

	I.say('Проверяю, что линия Калужско-Рижская свернута, кликая на линию Калужско-Рижская')
	I.click('Калужско-Рижская', '[data-marker="metro-select-dialog/lines"]')

	I.say('Проверяю что выбранная станция "Академическая" активна')
	I.see('Академическая', '[data-marker="metro-select-dialog/lines/station/toggle"]:checked ~ span')
})

Scenario('Проверяю кнопку Сброс', ({ I }) => {
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Выбираю станцию метро "Академическая"')
	I.click('Академическая')

	I.say('Проверяю что кнопка Сбросить активна')
	I.seeElement('[data-marker="metro-select-dialog/reset"]:not(:disabled)')

	I.say('Убираю выбор Станции "Академическая"')
	I.click('Академическая')

	I.say('Проверяю, что кнопка Сбросить неактивна')
	I.seeElement('[data-marker="metro-select-dialog/reset"]:disabled')
})

Scenario('После выбора станции метро через поисковую строку, поиск закрывается', ({ I }) => {
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Пишу в поисковую строку слово "Академическая"')
	I.fillField('[data-marker="metro-select-dialog/search"]', 'Академическая')

	I.say('Выбираю станцию Академическая')
	I.click('Академическая')

	I.say('Проверяю наличие табов')
	I.seeElement('[data-marker="metro-select-dialog/tabs"]')
})

Scenario('Проверяю наличие кнопки Применить', ({ I }) => {
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Выбираю станцию Академическая')
	I.click('Академическая')

	I.say('Проверяю текст на кнопке')
	I.see('Выбрать 1 станцию', '[data-marker="metro-select-dialog/apply"]')

	I.say('Выбираю станции Кутузовская и Киевская')
	I.click('Кутузовская')
	I.click('Киевская')

	I.say('Проверяю текст на кнопке')
	I.see('Выбрать 3 станции', '[data-marker="metro-select-dialog/apply"]')
})

Scenario('Проверяем добавление/удаление тегов с текстом выбранной станции метро', ({ I }) => {
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Выбираю станции Кутузовская')
	I.click('Кутузовская')
	I.wait(2)

	I.say('Проверяю наличие тега с текстом Кутузовская')
	I.see('Кутузовская', '[data-marker="metro-select-dialog/chips"]')

	I.say('Нажимаю на крестик в теге')
	I.click('[data-marker="metro-select-dialog/chips/wrapper"] [role="button"]')

	I.say('Проверяю что тег с текстом Кутузовская удалился')
	I.dontSeeElement('Кутузовская', '[data-marker="metro-select-dialog/chips/wrapper"]')

	I.say('Проверяю что станция Кутузовская не выбрана')
	I.see('Кутузовская', '[data-marker="metro-select-dialog/stations/item/toggle"]:not(:checked) ~ span')
})

Scenario('Проверяю открытие/закрытие фильтра метро без выбора', ({ I }) => {
	I.say('В фильтре уточнить нажимаю на метро')
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Проверяю что открылся фильтр метро')
	I.seeElement('[data-marker="metro-select-dialog"]')

	I.say('ничего не выбрав нажимаю стрелку назад')
	I.click('[data-marker="metro-select-dialog/back"]')

	I.say('Проверяю что не вижу фильтр метро')
	I.dontSee('[data-marker="metro-select-dialog"]')

	I.say('Проверяю что не было ничего выбрано')
	I.seeElement('[data-marker="metro-select/withoutValue"]')
})

Scenario('Проверяю открытие/закрытие фильтра метро с выбором станции', ({ I }) => {
	I.say('В фильтре уточнить нажимаю на метро')
	I.click('[data-marker="metro-select/withoutValue"]')

	I.say('Проверяю что открылся фильтр метро')
	I.seeElement('[data-marker="metro-select-dialog"]')

	I.say('выбираю станцию метро Кутузовская')
	I.click('Кутузовская')

	I.say('нажимаю на кнопку применить')
	I.click('[data-marker="metro-select-dialog/apply"]')

	I.say('Проверяю что не вижу фильтр метро')
	I.dontSee('[data-marker="metro-select-dialog"]')

	I.say('Проверяю что фильтр метро применён')
	I.see('Кутузовская', '[data-marker="metro-select/value"]')
})
